# Panelizer Usage Report

## Overview

This module provides an administrative page where you can select a certain Panels pane, and list all of the pages where it is being displayed on the site through Panelizer. It also provides another page, specifically geared towards Fieldable Panels Panes, that lists all the pages where a search phrase is found in text fields in fieldable panels panes.

## Installation

Download and enable the module as normal. For example:

```
drush dl panelizer_usage_report
drush en panelizer_usage_report
```

## Usage

After enabling the module, the pages can be found at:

/admin/reports/panelizer-usage-report/pane-usage

Select the pane you want to search for and click "Submit".

/admin/reports/panelizer-usage-report/fpp-content

Select the type of FPP, enter a search phrase, and click "Submit".
