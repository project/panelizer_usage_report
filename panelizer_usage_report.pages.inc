<?php
/**
 * @file
 * Page callback for the Panelizer Usage Report.
 */

function panelizer_usage_report_form($form, &$form_state) {

  // Construct the selection widget for panes.
  ctools_include('common', 'panels');
  $types = panels_common_get_allowed_types('panels_page');
  $options = array();
  foreach ($types as $type => $subtypes) {
    foreach ($subtypes as $subtype => $info) {
      $options[$subtype] = $info['title'];
    }
  }
  asort($options);

  $form['pane'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Select a pane to search for'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  // If the user has submitted this form, do the database queries.
  if (!empty($form_state['input']['pane'])) {
    $subtype = $form_state['input']['pane'];
    $form['pane']['#default_value'] = $subtype;

    $query = db_select('panelizer_entity', 'pe');
    $query->join('panels_pane', 'pp', 'pe.did = pp.did');
    $entities = $query
      ->condition('pp.subtype', $subtype)
      ->groupBy('pe.entity_type')
      ->groupBy('pe.entity_id')
      ->fields('pe', array('entity_type', 'entity_id'))
      ->execute()
      ->fetchAll();

    if (empty($entities)) {

      // If we found nothing, it might be a fieldable_panels_pane.
      if (module_exists('fieldable_panels_panes')) {

        $fpid_and_uuid = db_select('fieldable_panels_panes', 'fpp')
          ->fields('fpp', array('fpid', 'uuid'))
          ->condition('bundle', $subtype)
          ->execute()
          ->fetchAllKeyed();
        if (!empty($fpid_and_uuid)) {

          // If this is a fieldable panels pane search, start over with that
          // in mind.
          $subtypes = array();
          foreach ($fpid_and_uuid as $fpid => $uuid) {
            // Fieldable Panel Panes seem to appear in panels_pane with a sub-
            // type of either uuid:[uuid] or current:[fpid]. I don't know what
            // determines this difference in storage.
            $subtypes[] = 'uuid:' . $uuid;
            $subtypes[] = 'current:' . $fpid;
          }

          $query = db_select('panelizer_entity', 'pe');
          $query->join('panels_pane', 'pp', 'pe.did = pp.did');
          $entities = $query
            ->condition('pp.subtype', $subtypes)
            ->groupBy('pe.entity_type')
            ->groupBy('pe.entity_id')
            ->fields('pe', array('entity_type', 'entity_id'))
            ->execute()
            ->fetchAll();
        }
      }
    }

    $title = t('Entities that use this pane');
    $empty = t('There were no entities found that use this pane.');
    $form['links'] = _panelizer_usage_report_entity_list($entities, $title, $empty);
  }

  return $form;
}

/**
 * Submit callback for the panelizer_usage_report form.
 */
function panelizer_usage_report_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Page callback for a fieldable panels panes content search form.
 */
function panelizer_usage_report_fpp_content_form($form, &$form_state) {

  if (!module_exists('fieldable_panels_panes')) {
    return t('This page requires the Fieldable Panels Panes module.');
  }

  $bundle_info = field_info_bundles('fieldable_panels_pane');
  $bundles = array();
  foreach ($bundle_info as $key => $info) {
    $bundles[$key] = $info['label'];
  }

  $form['bundle'] = array(
    '#type' => 'select',
    '#options' => $bundles,
    '#title' => t('Select a type of fieldable panels panes to search in.'),
    '#required' => TRUE,
  );
  $form['pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter text to search for in these fieldable panels panes.'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  // If the user has submitted this form, do the database queries.
  if (!empty($form_state['input']['pattern'])) {

    $title = t('Entities with panes containing this text');
    $empty = t('There were no entities found that contained this text.');
    $entities = array();

    $pattern = '%' . db_like($form_state['input']['pattern']) . '%';
    $bundle = $form_state['input']['bundle'];
    $instances = field_info_instances('fieldable_panels_pane', $bundle);
    $fields = array();
    foreach ($instances as $field_name => $instance) {
      if (!empty($instance['widget']['module']) && 'text' === $instance['widget']['module']) {
        $fields[] = $field_name;
      }
    }
    if (empty($fields)) {
      $empty = t('That type of pane does not have text fields to search.');
    }
    else {
      $query = db_select('fieldable_panels_panes', 'fpp');
      $query->condition('fpp.bundle', $bundle);
      $or = db_or();
      foreach ($fields as $field) {
        $table = 'field_data_' . $field;
        $column = $field . '_value';
        $alias = $field;
        $query->join($table, $alias, 'fpp.fpid = ' . $alias . '.entity_id');
        $or->condition($alias . '.' . $column, $pattern, 'LIKE');
      }
      $entities = array();
      $fpid_and_uuid = $query
        ->fields('fpp', array('fpid', 'uuid'))
        ->condition($or)
        ->execute()
        ->fetchAllKeyed();
      if (!empty($fpid_and_uuid)) {
        $subtypes = array();
        foreach ($fpid_and_uuid as $fpid => $uuid) {
          // Fieldable Panel Panes seem to appear in panels_pane with a sub-
          // type of either uuid:[uuid] or current:[fpid]. I don't know what
          // determines this difference in storage.
          $subtypes[] = 'uuid:' . $uuid;
          $subtypes[] = 'current:' . $fpid;
        }
        $entity_query = db_select('panelizer_entity', 'pe');
        $entity_query->join('panels_pane', 'pp', 'pe.did = pp.did');
        $entities = $entity_query
          ->condition('pp.subtype', $subtypes)
          ->groupBy('pe.entity_type')
          ->groupBy('pe.entity_id')
          ->fields('pe', array('entity_type', 'entity_id'))
          ->execute()
          ->fetchAll();
      }
    }

    $form['links'] = _panelizer_usage_report_entity_list($entities, $title, $empty);
  }

  return $form;
}

/**
 * Submit callback for the panelizer_usage_report content form.
 */
function panelizer_usage_report_fpp_content_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Helper method to generate the output of the list of entities.
 *
 * @TODO: Make this into a sortable table with more columns.
 */
function _panelizer_usage_report_entity_list($entities, $title, $empty) {

  if (empty($entities)) {
    return array('#markup' => '<p>' . $empty . '</p>');
  }

  $header = array(
    array('data' => t('Label')),
    array('data' => t('Link')),
  );

  $rows = array();
  foreach ($entities as $info) {
    $entity = entity_load($info->entity_type, array($info->entity_id));
    $entity = array_shift($entity);
    $uri = entity_uri($info->entity_type, $entity);
    $label = entity_label($info->entity_type, $entity);
    $url = url($uri['path']);
    $rows[] = array($label, l($url, $url));
  }

  $prefix = '<h2>' . $title . '</h2>';
  $prefix .= '<p>Displaying ' . count($entities) . ' entities</p>';
  return array(
    '#prefix' => $prefix,
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array(
      // Support tablesorter if it's available.
      'class' => array('tablesorter'),
    )
  );
}
